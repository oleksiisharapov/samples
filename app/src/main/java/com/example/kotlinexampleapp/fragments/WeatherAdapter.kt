package com.example.kotlinexampleapp.fragments

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.kotlinexampleapp.R
import com.example.kotlinexampleapp.data.Weather
import kotlinx.android.synthetic.main.weather_item.view.*

class WeatherAdapter(val cities: List<Weather>, val context: Context) :
    RecyclerView.Adapter<MyViewHolder>() {


    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder?.weatherItem?.text = cities.get(position).toString()
    }


    override fun getItemCount(): Int {
        return cities.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(LayoutInflater.from(context).inflate(R.layout.weather_item, parent, false))
    }

}

    class MyViewHolder(view: View): RecyclerView.ViewHolder(view){

        val weatherItem = view.weather_item

    }


